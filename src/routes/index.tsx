import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import { HomePage } from '../pages/HomePage';
import { NewProject } from '../pages/project/NewProject';
import { ProjectProvider } from '../context/ProjectContext';

export const Routes = () => {
  return (
    <ProjectProvider>
      <div className="flex flex-row">
        <Router>
          <Switch>
            <Route path="/project/:id" component={NewProject} />
            <Route path="/" component={HomePage} />
          </Switch>
        </Router>
      </div>
    </ProjectProvider>
  );
};
