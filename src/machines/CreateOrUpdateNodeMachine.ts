import { assign, createMachine, Sender } from 'xstate';
import { ObjectNode, UnitNode } from '../models/hierarchy';
import { getStorageValue } from '../utility/getStorageValue';

export interface CreateOrUpdateNodeMachineContext {
  isInEditMode: boolean;
  hideForm: boolean;
  errorMessage?: string;
  itemToProcess?: ObjectNode;
  selectedNode?: ObjectNode;
  graph: UnitNode[];
};

export type CreateOrUpdateNodeMachineEvent =
  | {
      type: 'SUBMIT';
      item: ObjectNode;
    }
  | {
      type: 'SUCCESSFULLY_FETCHED_ITEM';
      item: ObjectNode;
    }
  | {
      type: 'UPDATE_GRAPH';
      item: UnitNode[];
    }
  | {
      type: 'RETRY';
    }
  | {
      type: 'EDIT_MODE';
      item: ObjectNode;
    }
  | {
      type: 'ADD_MODE';
      item: ObjectNode;
    }
  | {
      type: 'ADD_ROOT_MODE';
      item: ObjectNode;
    }
  | {
      type: 'RESET';
    }
  | {
      type: 'DELETE';
    };

const createOrUpdateNodeMachine = createMachine<CreateOrUpdateNodeMachineContext,CreateOrUpdateNodeMachineEvent>(
  {
    id: 'createOrUpdateNode',
    initial: 'fetchingItemToEdit',
    context: {
      isInEditMode: false,
      graph: [], 
      hideForm: false
    },
    on: {
      EDIT_MODE: {
        target: 'awaitingSubmit',
        actions: 'setToEditMode'
      },
      ADD_MODE: {
        target: 'awaitingSubmit',
        actions: ['resetContext', 'setToAddMode']
      },
      ADD_ROOT_MODE: {
        target: 'awaitingSubmit',
        actions: ['resetContext', 'setToAddMode']
      },
      UPDATE_GRAPH: {
        target: 'awaitingSubmit',
        actions: ['assignGraphToContext', 'resetContext']
      },
      RESET: {
        target: 'awaitingSubmit',
        actions: 'resetContext'
      }
    },
    states: {
      checkingIfInEditMode: {
        always: [
          {
            cond: 'isInEditMode',
            target: 'fetchingItemToEdit'
          },
          {
            target: 'awaitingSubmit'
          }
        ]
      },
      fetchingItemToEdit: {
        id: 'fetchingItemToEdit',
        invoke: {
          src: 'fetchItem',
          onError: {
            target: 'failedToFetch',
            actions: 'assignErrorMessageToContext'
          }
        },
        on: {
          SUCCESSFULLY_FETCHED_ITEM: {
            target: 'awaitingSubmit',
            actions: 'assignItemToContext'
          }
        }
      },
      failedToFetch: {
        exit: ['clearErrorMessage'],
        on: {
          RETRY: 'fetchingItemToEdit'
        }
      },
      awaitingSubmit: {
        id: 'awaitingSubmit',
        exit: ['clearErrorMessage'],
        on: {
          SUBMIT: {
            target: 'submitting',
            actions: ['assignItemToContext']
          },
          DELETE: {
            target: 'deleting'
          }
        }
      },
      deleting: {
        invoke: {
          src: 'deleteItem'
        }
      },
      submitting: {
        initial: 'checkingIfInEditMode',
        states: {
          checkingIfInEditMode: {
            always: [
              {
                cond: 'isInEditMode',
                target: 'editing'
              },
              {
                target: 'creating'
              }
            ]
          },
          editing: {
            invoke: {
              src: 'editItem',
              onDone: { target: '#awaitingSubmit', actions: 'onEditSuccess' },
              onError: {
                target: '#awaitingSubmit',
                actions: ['assignErrorMessageToContext', 'resetContext']
              }
            }
          },
          creating: {
            invoke: {
              src: 'createItem',
              onDone: { target: '#awaitingSubmit', actions: 'onCreateSuccess' },
              onError: {
                target: '#awaitingSubmit',
                actions: ['assignErrorMessageToContext', 'resetContext']
              }
            }
          }
        }
      },
    }
  },
  {
    guards: {
      isInEditMode: (context: CreateOrUpdateNodeMachineContext) => {
        return context.isInEditMode;
      }
    },
    services: {},
    actions: {
      onCreateSuccess: () => {
        console.log('created');
        alert('onCreateSuccess');
      },
      onEditSuccess: () => {
        alert('onEditSuccess');
      },
      clearErrorMessage: assign((context: CreateOrUpdateNodeMachineContext) => ({
        errorMessage: undefined
      })),
      setToEditMode: assign(
        (context: CreateOrUpdateNodeMachineContext, event: CreateOrUpdateNodeMachineEvent) => {
          if (event.type !== 'EDIT_MODE') return {};
          return {
            isInEditMode: true,
            selectedNode: event.item,
            hideForm: false
          };
        }
      ),
      setToAddMode: assign(
        (context: CreateOrUpdateNodeMachineContext, event: CreateOrUpdateNodeMachineEvent) => {
          if (event.type !== 'ADD_MODE' && event.type !== 'ADD_ROOT_MODE') return {};
          return {
            isInEditMode: false,
            hideForm: false
          };
        }
      ),
      resetContext: assign(
        (context: CreateOrUpdateNodeMachineContext, event: CreateOrUpdateNodeMachineEvent) => {
          if (event.type === 'ADD_MODE')
            return {
              isInEditMode: false,
              errorMessage: '',
              itemToProcess: undefined,
              hideForm: false
            };
          if (event.type === 'ADD_ROOT_MODE')
            return {
              isInEditMode: false,
              errorMessage: '',
              selectedNode: undefined,
              itemToProcess: undefined,
              hideForm: false
            };

          return {
            isInEditMode: false,
            errorMessage: '',
            selectedNode: undefined,
            itemToProcess: undefined,
            hideForm: true
          };
        }
      ),
      assignErrorMessageToContext: assign(
        (context: CreateOrUpdateNodeMachineContext, event: CreateOrUpdateNodeMachineEvent) => {
          console.log(event);
          return {
            errorMessage: event.type || 'An unknown error occurred'
          };
        }
      ),
      assignItemToContext: assign((context, event) => {
        if (event.type !== 'SUCCESSFULLY_FETCHED_ITEM' && event.type !== 'SUBMIT') return {};
        return {
          itemToProcess: event.item,
          hideForm: false
        };
      }),
      assignGraphToContext: assign((context, event) => {
        if (event.type !== 'UPDATE_GRAPH') return {};
        //add graph to local storage as well
        localStorage.setItem('graph', JSON.stringify(event.item));
        return {
          graph: event.item
        };
      })
    }
  }
);

export default createOrUpdateNodeMachine;
