import React from 'react';
import { assign, createMachine, Sender } from 'xstate';

export interface ProjectNavigationMachineContext {
  projectName: string;
  page?: string;
}

export type ProjectNavigationMachineEvent =
  | {
      type: 'NEXT';
    }
  | {
      type: 'PREV';
    }
  | {
      type: 'GO_TO_TARGET';
      page: string;
    };

const projectNavigationMachine = createMachine<
  ProjectNavigationMachineContext,
  ProjectNavigationMachineEvent
>(
  {
    id: 'projectNavigation',
    initial: 'idle',
    context: {
      projectName: ''
    },
    states: {
      idle: {
        on: {
          GO_TO_TARGET: {
            actions: 'goToTargetPage'
          }
        }
      }
    }
  },
  {
    actions: {
      goToTargetPage: assign((context, event) => {
        if (event.type !== 'GO_TO_TARGET') return {};
        return {
          page: event.page
        };
      })
    }
  }
);

export default projectNavigationMachine;
