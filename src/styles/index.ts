export const styles = {
  menu: {
    item: 'hover:bg-secondary p-4 cursor-pointer text-white text-sm',
    itemActive: 'bg-secondary cursor-pointer text-white',
    itemInactive: 'bg-primary cursor-pointer text-white'
  },
  button: {
    circle: 'rounded-full h-12 w-12',
    rectangle: 'px-4 py-4 shadow m-1',
    rounded_rectangle: 'rounded outline-none px-4 py-4 shadow mr-1 mb-1',
    active: '',
    inactive: '',
    primary: 'bg-primary',
    secondary: 'bg-secondary',
    transparent: 'bg-transparent',
    normal: 'text-white flex items-center justify-center'
  },
  unitNode: {
    normal:
      'flex flex-col p-4 bg-secondary bg-opacity-70 text-white text-md m-2 border-2 border-secondary',
    selected:
      'flex flex-col p-4 bg-secondary bg-opacity-70 text-white text-md m-2 border-2 border-yellow-400 shadow-lg'
  },
  actuatorNode: {
    normal: 'flex flex-col p-4',
    selected: 'flex flex-col p-4 text-bold font-bold'
  }, 
  content : "flex flex-col w-full h-5/6 justify-between",
  pageContent: "flex flex-col w-4/5 align-middle mx-auto my-8"
};
