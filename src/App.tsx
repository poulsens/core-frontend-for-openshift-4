import { createClient, Provider } from 'urql';
import { Routes } from './routes';
import './App.css';

const host = component_backend_host ;

const port = component_backend_port ;

const connection_url = 'http://' + host + ':' + port + '/graphql';
console.log('Creating client front-end on:');
console.log(connection_url);

const client = createClient({
  url:  connection_url,
  fetchOptions: () => {
    return {
      headers: { "Access-Control-Allow-Origin" : "*"   }
    };
  }
});


function App() {
  return (
    <Provider value={client}>
      <Routes />
    </Provider>
  );
}

export default App;
