import { gql } from '@urql/core';

export const AddActuatorToProject = gql`
  mutation ($projectId: String!, $actuator: ActuatorInput!) {
    AddActuatorToPlant(plantId: $projectId, actuator: $actuator) {
      actuators {
        _id
        shortName
      }
    }
  }
`;

export const UpdateActuatorInProject = gql`
  mutation ($projectId: String!, $actuator: ActuatorInput!) {
    UpdateActuatorInPlant(plantId: $projectId, actuator: $actuator) {
      units {
        _id
        shortName
      }
    }
  }
`;

export const RemoveActuatorInProject = gql`
  mutation ($projectId: String!, $actuatorId: String!) {
    RemoveActuatorInPlant(plantId: $projectId, actuatorId: $actuatorId) {
      actuators {
        _id
        shortName
      }
    }
  }
`;
