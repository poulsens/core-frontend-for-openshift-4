import { gql } from '@urql/core';

export const AddUnitToProject = gql`
  mutation ($projectId: String!, $unit: UnitInput!) {
    AddUnitToPlant(plantId: $projectId, unit: $unit) {
      units {
        _id
        shortName
      }
    }
  }
`;

export const UpdateUnitInProject = gql`
  mutation ($projectId: String!, $unit: UnitInput!) {
    UpdateUnitInPlant(plantId: $projectId, unit: $unit) {
      units {
        _id
        shortName
      }
    }
  }
`;

export const RemoveUnitInProject = gql`
  mutation ($projectId: String!, $unitId: String!) {
    RemoveUnitInPlant(plantId: $projectId, unitId: $unitId) {
      units {
        _id
        shortName
      }
    }
  }
`;
