import { ObjectNode } from '../models/hierarchy';

export const isObjectUnit = (obj: ObjectNode) => {
  if (obj && obj.hasOwnProperty('children')) {
    return true;
  }
  return false;
};
