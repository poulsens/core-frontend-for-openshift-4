import { v4 as uuidv4 } from 'uuid';
import { ObjectId } from 'bson';

export const generateObjectId = () => {
  const id = new ObjectId();
  return id.toString();
};
