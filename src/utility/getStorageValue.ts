export const getStorageValue = (key: string, defaultValue: any) => {
  const saved = localStorage.getItem(key);
  //@ts-ignore
  const initial = JSON.parse(saved);
  return initial || defaultValue;
};
