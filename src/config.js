



// Fetch config variables defined in window.*
//
const envSettings = window;
//
//
//
export const configuration = {
//
   component_backend_host: envSettings.COMPONENT_BACKEND_HOST || '0.0.0.0'

   component_backend_port: envSettings.COMPONENT_BACKEND_PORT || '8080'
//
}
//
//
//
//
