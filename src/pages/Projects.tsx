import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useQuery, gql, useMutation } from 'urql';
import { Button } from '../components/Button';
import { addIcon } from '../components/Icons';
import { Modal } from '../components/Modal';
import { SearchBar } from '../components/SearchBar';
import { TextInput } from '../components/TextInput';
import { ButtonShape, ButtonType } from '../models/ui';
import { ProjectCard } from './project/ProjectCard';
import { useProject } from '../context/ProjectContext';

const ProjectsQuery = gql`
  query {
    plantMany {
      _id
      name
    }
  }
`;

const AddProjectMutation = gql`
  mutation ($projectName: String!) {
    PlantCreateOne(record: { name: $projectName }) {
      record {
        _id
        name
      }
    }
  }
`;

export const Projects: React.FC = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [visibility, setVisibility] = useState(false);
  const [projectNameLocal, setProjectNameLocal] = useState('');
  const history = useHistory();

  const [result, reexecuteQuery] = useQuery({
    query: ProjectsQuery
  });

  const [addProjectResult, addProject] = useMutation(AddProjectMutation);
  const { setProjectName, setProjectId } = useProject();

  const { data, fetching, error } = result;

  const addPlant = () => {
    setVisibility(true);
    setProjectNameLocal('');
  };

  const submitModal = () => {
    const variables = { projectName: projectNameLocal };
    addProject(variables).then((result) => {
      const { data, error } = result;
      if (error)
        //TODO: Add error message here
        return;
      const { name, _id } = data.PlantCreateOne.record;
      setProjectName(name);
      setProjectId(_id);
      const cleanName = name.replace(/ /g, '');
      setVisibility(false);
      history.push(`/project/${cleanName}`);
    });
  };

  const addProjectForm = () => {
    return (
      <TextInput
        value={projectNameLocal}
        setValue={setProjectNameLocal}
        placeholder="Project Name"
        id="projectName"
      />
    );
  };

  return (
    <div className="flex flex-col w-full">
      <header className="flex flex-row justify-between p-8">
        <SearchBar
          value={searchQuery}
          setValue={setSearchQuery}
          placeholder="Search Plants"
          id="search"
        />
        <Button
          shape={ButtonShape.circle}
          onClick={addPlant}
          icon={addIcon}
          type={ButtonType.secondary}
        />
      </header>
      <div className="flex flex-row justify-start flex-wrap">
        {fetching && <p> Loading...</p>}
        {error && <p> Oh no... {error.message}</p>}
        {data &&
          data.plantMany.map((project: any) => (
            <ProjectCard
              id={project._id}
              key={project._id}
              title={project.name}
              route={`/project/${project.name.replace(/ /g, '')}`}
            />
          ))}
      </div>
      {visibility ? (
        <Modal
          visibility={visibility}
          setVisibility={setVisibility}
          title="Add new project"
          content={addProjectForm()}
          submitModal={submitModal}
        />
      ) : null}
    </div>
  );
};
