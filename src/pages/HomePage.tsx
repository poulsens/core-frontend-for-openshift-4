import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { SideBar } from '../components/SideBar';
import { SideBarItemProps } from '../models/ui';
import { Projects } from './Projects';
import { Standards } from './Standards';

const items: Array<SideBarItemProps> = [
  {
    title: 'Projects',
    route: '/projects'
  },
  {
    title: 'Standards',
    route: '/standards'
  }
];

export const HomePage = () => {
  return (
    <div className="flex flex-row min-w-full">
      <SideBar items={items} title="CORE">
        <Switch>
          <Route exact path="/">
            <Redirect to="/projects" />
          </Route>
          <Route exact path="/projects" component={Projects} />
          <Route exact path="/standards" component={Standards} />
        </Switch>
      </SideBar>
    </div>
  );
};
