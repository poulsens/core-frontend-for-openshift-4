import { useState } from 'react';
import { TextArea } from '../../components/TextArea';
import { TextEditor } from '../../components/Editor/TextEditor';
import { Descendant } from 'slate';
import { ButtonShape, ButtonType, CustomElement } from '../../models/ui';
import { Button } from '../../components/Button';
import { addIcon } from '../../components/Icons';
import { PageTitle } from '../../components/PageTitle';

export const Introduction: React.FC = () => {
  const initialValue: CustomElement[] = [
    {
      type: 'paragraph',
      children: [
        {
          text: ''
        }
      ]
    }
  ];

  const [purpose, setPurpose] = useState<Descendant[]>(initialValue);
  const [generalCapabilities, setGeneralCapabilities] = useState<Descendant[]>(initialValue);
  const [dysfunctionalCapabilities, setDysfunctionalCapabilities] =
    useState<Descendant[]>(initialValue);

  const addFiremanCubicle = () => {
    console.log('add fireman cubicle');
  };

  return (
    <div className="flex flex-col w-4/5 align-middle mx-auto my-8">
      <PageTitle title="Introduction" /> 
      <TextEditor placeholder="Purpose" value={purpose} setValue={setPurpose} />
      <TextEditor
        placeholder="General Capabilities"
        value={generalCapabilities}
        setValue={setGeneralCapabilities}
      />
      <TextEditor
        placeholder="Dysfunctional Capabilities"
        value={dysfunctionalCapabilities}
        setValue={setDysfunctionalCapabilities}
      />
      <div className="flex flex-row w-full my-8">
        <Button
          type={ButtonType.primary}
          shape={ButtonShape.rectangle}
          onClick={addFiremanCubicle}
          text="Fireman Cubicle"
          icon={addIcon}
        />
        <Button
          type={ButtonType.primary}
          shape={ButtonShape.rectangle}
          onClick={addFiremanCubicle}
          text="Hardcoded Signal"
          icon={addIcon}
        />
        <Button
          type={ButtonType.primary}
          shape={ButtonShape.rectangle}
          onClick={addFiremanCubicle}
          text="Software Signal"
          icon={addIcon}
        />
      </div>
    </div>
  );
};
