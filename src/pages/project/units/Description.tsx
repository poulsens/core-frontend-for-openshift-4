import React, { useState, useEffect } from "react"
import { useParams } from "react-router"
import { useQuery } from "urql"
import { PageTitle } from "../../../components/PageTitle"
import { TextArea } from "../../../components/TextArea"
import { useProject } from "../../../context/ProjectContext"
import { GetUnitDescriptionByName } from "../../../queries/unit"
import { styles } from "../../../styles"

export const UnitDescription : React.FC = () => { 
    //@ts-ignore
    const { unitName } = useParams()
    const { projectId } = useProject() 
    const [description, setDescription] = useState("")
    const [unitDescription, reexecuteUnitDescription] = useQuery({
        query: GetUnitDescriptionByName, 
        variables: {projectId, unitName}, 
        pause: !projectId || !unitName
    })

    const { data, fetching, error } = unitDescription; 
    console.log(fetching, error)
    console.log(projectId, unitName)
    useEffect(() => { 
        if(data){ 
            setDescription(data.unitByName.description)
        }
    }, [data])

    return( 
        <div className={styles.pageContent}> 
            <PageTitle title={`Description of ${unitName}`} />
            <TextArea id="unit_description" value={description} setValue={setDescription} placeholder="Unit Description" />
        </div>
    )
}