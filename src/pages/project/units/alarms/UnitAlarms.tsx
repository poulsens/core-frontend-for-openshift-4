import { useState } from "react"
import { PageTitle } from "../../../../components/PageTitle"
import { Table } from "../../../../components/Table"
import { styles } from "../../../../styles"

export const UnitAlarms : React.FC = () => {
    const cols = [
        {
            title: "Name",
            index: "name",
        }, 
        {
            title: "Condition",
            index: "condition",
        }, 
        { 
            title: "Temporisation",
            index: 'temporisation'
        }, 
        { 
            title: "Message",
            index: 'message'
        },       
        { 
            title: "Action",
            index: 'action'
        },  
        {
            title: "CCC grounp",
            index: "cccGroup",
        }, 
    ]


    const [items, setItems] = useState([])
    return ( 
        <div className={styles.pageContent}> 
            <PageTitle title="Unit Alarms" />
            <Table cols={cols} items={items} setItems={setItems} size={0} />
        </div>
    )
}