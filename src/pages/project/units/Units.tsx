import { useQuery } from "urql";
import { TitleCard } from "../../../components/TitleCard";
import { useProject } from "../../../context/ProjectContext";
import { AllUnitNames, AllUnitsQuery } from "../../../queries/unit";
import { styles } from "../../../styles";

export const Units: React.FC = () => {
  const {projectId, projectName} = useProject()
  const [allUnits, reexecuteAllUnits] = useQuery({
    query: AllUnitNames,
    variables: {projectId}, 
    pause: projectId === null, 
  })
  const { data, fetching, error} = allUnits;
  return (
    <div className={styles.content}>
      {fetching && <p> Loading...</p>}
      {error && <p>{error.message}</p>}
      <div className="flex flex-row justify-start flex-wrap">
      {data && data.plantById.units.map((unit : {shortName: string, _id: string}) => <TitleCard title={unit.shortName} id={unit._id} key={unit._id} route={`/project/${projectName}/units/${unit.shortName}`} />)}
      </div>
    </div>
    //get all units from the mongo db
  );
};
