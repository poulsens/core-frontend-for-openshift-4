import React , { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import { useQuery } from 'urql';
import { PageTitle } from '../../../components/PageTitle';
import { Table } from '../../../components/Table';
import { useProject } from '../../../context/ProjectContext';
import { GetChildActuatorsOfUnit } from '../../../queries/actuator';
import { GetChildUnitsOfUnit, GetOperationalStateNamesByUnitName } from '../../../queries/unit';
import { styles } from '../../../styles';

const _cols = [
    { 
        title: 'States', 
        index: 'states',
    }
] 

export const ObjectBehaviour : React.FC = () => {
    const { projectId } = useProject() 
    const { unitName } = useParams<{unitName: string}>()

    const [ operationalStates, setOperationStates ] = useState([]);
    const [ children, setChildren ] = useState([]);
    const [ cols, setCols ] = useState(_cols)
    const [ items, setItems ] = useState([]);

    const [operationStateNames, reexecuteOperationStateName] = useQuery({
        query: GetOperationalStateNamesByUnitName, 
        variables: {projectId, unitName}, 
        pause: !projectId || !unitName
    })

    const [childUnits, reexecuteChildUnits] = useQuery({
        query: GetChildUnitsOfUnit, 
        variables: {projectId, unitName},
        pause: !projectId || !unitName
    })

    const [childActuators, reexecuteChildActuators] = useQuery({
        query: GetChildActuatorsOfUnit, 
        variables: {projectId, unitName},
        pause: !projectId || !unitName
    })

    useEffect(() => { 
        if(operationStateNames.data) { 
            setOperationStates(operationStateNames.data.operationalStates)
        }
        if(childUnits.data && childActuators.data) { 
            const _children = [...childUnits.data.childUnitsOfUnit, ...childActuators.data.childActuatorsOfUnit]
            //@ts-ignore
            setChildren(_children)
            _children.forEach((child : {shortName: string, _id: string}) => {
                const existing = cols.filter((col : {index: string}) => col.index === child._id)
                if(existing.length === 0) {
                    cols.push({ 
                        title: child.shortName, 
                        index: child._id,
                    })
                }
            })     
        }
        
    }, [operationStateNames.data, childUnits.data, childActuators.data])
    return (
        <div className={styles.pageContent}>
            <PageTitle title="Object Behaviour" />
            <Table cols={cols} items={items} setItems={setItems} size={0} />
        </div>
    );
}