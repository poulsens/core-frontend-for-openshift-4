import { useState, useEffect } from "react"
import { useParams } from "react-router"
import { useQuery } from "urql"
import { PageTitle } from "../../../components/PageTitle"
import { Table } from "../../../components/Table"
import { useProject } from "../../../context/ProjectContext"
import { GetOperationalStatesByUnitName, GetTransitionConditionsByUnitName } from "../../../queries/unit"
import { styles } from "../../../styles"

export const OperationalStates : React.FC = () => { 
    const { projectId } = useProject()
    //@ts-ignore
    const { unitName } = useParams() 
    const [ stateDefintions, setStateDefinitions ] = useState([])
    const [ transitionConditions, setTransitionConditions ] = useState([]) 
    const [operationalStatesQuery, reexecuteOperationalStatesQuery] = useQuery({
        query: GetOperationalStatesByUnitName, 
        variables: {projectId, unitName}, 
        pause: !projectId || !unitName,
        requestPolicy: 'cache-and-network',
    })
    const [transitionConditionsQuery, reexecuteTransitionConditionsQuery] = useQuery({
        query: GetTransitionConditionsByUnitName, 
        variables: {projectId, unitName}, 
        pause: !projectId || !unitName
    })
    const stateCols = [
        {
            title: "STEP", 
            index: "step"
        }, 
        {
            title: "NAME", 
            index: "name"
        }, 
        {
            title: "DESCRIPTION", 
            index: "description"
        }, 
       
    ]

    const conditionCols = [
        {
            title: "START", 
            index: "start"
        }, 
        {
            title: "STOP", 
            index: "stop"
        }, 
        {
            title: "NAME", 
            index: "name"
        }, 
        {
            title: "CONDITION", 
            index: "condition"
        }, 
    ]

    useEffect(() => { 
        if(operationalStatesQuery.data){
            console.log(operationalStatesQuery.data)
            //@ts-ignore
            setStateDefinitions(operationalStatesQuery.data.unitByName.operationalStates.map(({__id, __typename, ...attr}) => attr))
        }
        if(transitionConditionsQuery.data){
            //@ts-ignore
            setTransitionConditions(transitionConditionsQuery.data.unitByName.transitionConditions.map(({__id, __typename, ...attr}) => attr))
        }
    }, [operationalStatesQuery.data, transitionConditionsQuery.data])
    return ( 
        <div className={styles.pageContent}> 
            <PageTitle title="Operational States" /> 
            <h2> Operational State Definitions </h2>
            <Table cols={stateCols} items={stateDefintions} setItems={setStateDefinitions} size={0} />
            <h2> Transition Conditions </h2>
            <Table cols={conditionCols} items={transitionConditions} setItems={setTransitionConditions} size={0} />
        </div>
    )
}