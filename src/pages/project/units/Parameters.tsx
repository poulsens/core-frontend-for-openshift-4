import { useState } from "react"
import { PageTitle } from "../../../components/PageTitle"
import { Table } from "../../../components/Table"
import { styles } from "../../../styles"

export const Parameters : React.FC = () => {
    const cols = [
        {
            title: "Name",
            index: "name",
        }, 
        { 
            title: "Description",
            index: 'description'
        },       
        { 
            title: "Default Value",
            index: 'defaultValue'
        },  
        { 
            title: "Unit",
            index: 'unit'
        }, 
    ]

    const [items, setItems] = useState([])
    return ( 
        <div className={styles.pageContent}> 
            <PageTitle title="Paramters" />
            <Table cols={cols} items={items} setItems={setItems} size={0} />
        </div>
    )
}