import { useState } from "react"
import { PageTitle } from "../../../components/PageTitle"
import { Table } from "../../../components/Table"
import { styles } from "../../../styles"

export const ComputedVariables : React.FC = () => {
    const cols = [
        {
            title: "Name",
            index: "name",
        }, 
        {
            title: "Code",
            index: "code",
        }, 
        { 
            title: "Description",
            index: 'description'
        },       
        { 
            title: "Default Value",
            index: 'defaultValue'
        },  
        {
            title: "Type",
            index: "type",
        }, 
        { 
            title: "Unit",
            index: 'unit'
        }, 
    ]


    const [items, setItems] = useState([])
    return ( 
        <div className={styles.pageContent}> 
            <PageTitle title="Computed Variables" />
            <Table cols={cols} items={items} setItems={setItems} size={0} />
        </div>
    )
}