import { useState } from "react"
import { PageTitle } from "../../../components/PageTitle"
import { Table } from "../../../components/Table"
import { styles } from "../../../styles"

export const UserCommands : React.FC = () => {
    const cols = [
        {
            title: "Command",
            index: "command",
        }, 
        { 
            title: "Type",
            index: 'type'
        }, 
        { 
            title: "Description",
            index: 'description'
        },        
    ]

    const [items, setItems] = useState([])
    return ( 
        <div className={styles.pageContent}> 
            <PageTitle title="User Commands" />
            <Table cols={cols} items={items} setItems={setItems} size={0} />
        </div>
    )
}