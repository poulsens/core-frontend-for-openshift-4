import { TitleCard, TitleCardProps } from '../../components/TitleCard';
import { useProject } from '../../context/ProjectContext';

export const ProjectCard: React.FC<TitleCardProps> = (props) => {
  const { setProjectName, setProjectId } = useProject();
  const changeProject = () => {
    const cleanTitle = props.title.replace(/ /g, '');
    setProjectName(cleanTitle);
    setProjectId(props.id);
  };
  return (
    <div onClick={changeProject}>
      <TitleCard {...props} />
    </div>
  );
};
