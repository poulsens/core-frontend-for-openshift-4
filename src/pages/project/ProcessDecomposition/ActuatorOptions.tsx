import React, { useState } from 'react';
import { Dropdown } from '../../../components/Dropdown';
import { Table } from '../../../components/Table';
import {
  AnalogCommand,
  AnalogFeedback,
  Command,
  DigitalCommand,
  DigitalFeedback,
  FailSafeOptions,
  Feedback
} from '../../../models/hierarchy';

interface ActuatorOptionsProps {
  failSafe: FailSafeOptions;
  setFailSafe: (e: any) => void;
  digitalCommands: Array<DigitalCommand>;
  setDigitalCommands: (commands: DigitalCommand[]) => void;
  analogCommands: Array<AnalogCommand>;
  setAnalogCommands: (commands: AnalogCommand[]) => void;
  digitalFeedback: Array<DigitalFeedback>;
  setDigitalFeedback: (feedback: DigitalFeedback[]) => void;
  analogFeedback: Array<AnalogFeedback>;
  setAnalogFeedback: (feedback: AnalogFeedback[]) => void;
}
export const ActuatorOptions: React.FC<ActuatorOptionsProps> = ({
  failSafe,
  setFailSafe,
  digitalCommands,
  analogCommands,
  digitalFeedback,
  analogFeedback,
  setDigitalCommands,
  setAnalogCommands,
  setDigitalFeedback,
  setAnalogFeedback
}) => {
  const digitalCommandcols = [
    {
      title: 'Name',
      index: 'name'
    },
    {
      title: 'Description',
      index: 'description'
    },
    {
      title: 'Status',
      index: 'commandStatus',
      type: 'onOffRadio'
    },
    {
      title: 'Label ON',
      index: 'labelOn'
    },
    {
      title: 'Label OFF',
      index: 'labelOff'
    }
  ];

  const digitalFeedbackCols = [
    {
      title: 'Name',
      index: 'name'
    },
    {
      title: 'Description',
      index: 'description'
    },
    {
      title: 'Status',
      index: 'feedbackStatus',
      type: 'onOffRadio'
    }
  ];

  const analogCols = [
    {
      title: 'Name',
      index: 'name'
    },
    {
      title: 'Description',
      index: 'description'
    },
    {
      title: 'Range Min',
      index: 'rangeMin'
    },
    {
      title: 'Range Max',
      index: 'rangeMax'
    },
    {
      title: 'Unit',
      index: 'unit'
    }
  ];

  return (
    <>
      <Dropdown
        label="FailSafe"
        options={Object.keys(FailSafeOptions).map((key) => {
          //@ts-ignore
          return FailSafeOptions[key];
        })}
        defaultValue={failSafe}
        setValue={setFailSafe}
      />
      <p className="xl">Commands</p>
      <p className="lg">Digital</p>
      <Table
        cols={digitalCommandcols}
        items={digitalCommands}
        setItems={setDigitalCommands}
        size={0}
      />
      <p className="lg">Analog</p>
      <Table cols={analogCols} items={analogCommands} setItems={setAnalogCommands} size={0} />
      <p className="xl">Feedback</p>
      <p className="lg">Digital</p>
      <Table
        cols={digitalFeedbackCols}
        items={digitalFeedback}
        setItems={setDigitalFeedback}
        size={0}
      />
      <p className="lg">Analog</p>
      <Table cols={analogCols} items={analogFeedback} setItems={setAnalogFeedback} size={0} />
    </>
  );
};
