import { OnChangeValue, ActionMeta } from 'react-select';
import CreatableSelect from 'react-select/creatable';
import { DropdownOption } from '../../../models/ui';
import { useGraph } from '../../../context/GraphContext';

interface UnitsDropdownProps {
  shortName: string;
  setShortName: (name: string) => void;
}
export const UnitsDropdown: React.FC<UnitsDropdownProps> = ({ shortName, setShortName }) => {
  const { unitActuator } = useGraph();
  const getUnits = () => {
    const keys = Array.from(unitActuator.keys());
    const options: Array<DropdownOption> = [];
    keys.map((key) => options.push({ value: key, label: key }));
    return options;
  };

  const handleChange = (
    newValue: OnChangeValue<DropdownOption, false>,
    actionMeta: ActionMeta<DropdownOption>
  ) => {
    if (newValue) {
      setShortName(newValue.value);
    }
  };

  //@
  return (
    <CreatableSelect
      isClearable
      options={getUnits()}
      onChange={handleChange}
      className="mb-4"
      placeholder="Short Name"
      value={{ value: shortName, label: shortName }}
    />
  );
};
