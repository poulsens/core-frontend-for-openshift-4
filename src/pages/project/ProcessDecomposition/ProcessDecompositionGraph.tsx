import React from 'react';
import { ActuatorNode, ObjectNode, UnitNode } from '../../../models/hierarchy';
import { useGraph } from '../../../context/GraphContext';
import { Tree, TreeNode } from 'react-organizational-chart';
import { CreateOrUpdateNodeMachineContext } from '../../../machines/CreateOrUpdateNodeMachine';
import { styles } from '../../../styles';

interface SubTreeProps {
  node: ObjectNode;
}
export const ProcessDecompositionGraph: React.FC = () => {
  const { createOrUpdateNodeSendEvent, createOrUpdateNodeState } = useGraph();
  const { graph, selectedNode } =
    createOrUpdateNodeState.context as CreateOrUpdateNodeMachineContext;
  const changeSelectedParent = (node: UnitNode) => {
    if (selectedNode !== node) createOrUpdateNodeSendEvent({ type: 'EDIT_MODE', item: node });
  };

  const changeSelectedActuator = (node: ActuatorNode) => {
    createOrUpdateNodeSendEvent({ type: 'EDIT_MODE', item: node });
  };
  const UnitNode = (node: UnitNode) => (
    <div
      className={
        selectedNode?.attributes.id === node.attributes.id
          ? styles.unitNode.selected
          : styles.unitNode.normal
      }
      onClick={() => changeSelectedParent(node)}>
      <p> {node.name} </p>
      <p> {node.attributes.shortDescription} </p>
    </div>
  );
  const ActuatorNode = (node: ActuatorNode) => (
    <div
      onClick={() => changeSelectedActuator(node)}
      className={
        selectedNode?.attributes.id === node.attributes.id
          ? styles.actuatorNode.selected
          : styles.actuatorNode.normal
      }>
      {node.name}
    </div>
  );

  const SubTree: React.FC<SubTreeProps> = ({ node }) => {
    //@ts-ignore
    if (node.children === undefined) {
      return <TreeNode label={ActuatorNode(node as ActuatorNode)} />;
    }
    return (
      <TreeNode label={UnitNode(node as UnitNode)}>
        {
          //@ts-ignore
          node.children?.map((child) => (
            <SubTree node={child} key={child.attributes.id} />
          ))
        }
      </TreeNode>
    );
  };
  return (
    <div className="flex w-full h-full border-r-2 overflow-scroll">
      {graph.map((node: UnitNode) => (
        <Tree label={UnitNode(node)} key={node.attributes.id}>
          {node.children?.map((child) => (
            <SubTree node={child} key={child.attributes.id} />
          ))}
        </Tree>
      ))}
    </div>
  );
};
