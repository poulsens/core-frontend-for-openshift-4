import XLSX, { WorkSheet } from 'xlsx';
import {
  ActuatorNode,
  Command,
  Feedback,
  UnitNode,
  ActuatorType,
  FailSafeOptions,
  ObjectNode
} from '../../../models/hierarchy';
import { generateObjectId } from '../../../utility/generateUuid';
import { MapofStringtoStringSet } from '../../../context/GraphContext';

const commands = ['DO', 'AO', 'ODO', 'CDO'];
const feedback = ['ZT', 'ON', 'ZSH', 'ZSL'];
export const _digitalCommands = ['DO', 'ODO', 'CDO'];
export const _analogCommands = ['AO'];
export const _digitalFeedback = ['ON', 'ZSH', 'ZSL'];
export const _analogFeedback = ['ZT'];

export const addUnitNode = (
  shortName: string,
  displayName: string,
  shortDescription: string,
  description: string,
  unitActuator: MapofStringtoStringSet,
  actuatorCommand: MapofStringtoStringSet,
  actuatorFeedback: MapofStringtoStringSet
) => {
  const unitNode: UnitNode = {
    name: shortName,
    attributes: {
      displayName,
      shortDescription,
      description,
      id: generateObjectId()
    },
    children: []
  };
  if (unitActuator.has(shortName)) {
    const actuators = unitActuator.get(shortName);
    const children: ActuatorNode[] = [];
    actuators?.forEach((actuator) => {
      const command: Command[] = [];
      const feedback: Feedback[] = [];
      if (actuatorCommand.has(actuator)) {
        const _command = actuatorCommand.get(actuator);
        _command?.forEach((c) => {
          const _c = c.split(':');
          if (_digitalCommands.includes(_c[0])) {
            command.push({
              name: _c[0],
              description: _c[1],
              status: 'on',
              labelOn: 'ON',
              labelOff: 'OFF'
            });
          } else {
            command.push({
              name: _c[0],
              description: _c[1],
              rangeMin: parseInt(_c[2]),
              rangeMax: parseInt(_c[3]),
              unit: _c[4]
            });
          }
        });
      }
      if (actuatorFeedback.has(actuator)) {
        const _feedback = actuatorFeedback.get(actuator);
        _feedback?.forEach((c) => {
          const _c = c.split(':');
          if (_digitalFeedback.includes(_c[0])) {
            feedback.push({ name: _c[0], description: _c[1], status: 'on' });
          } else {
            feedback.push({
              name: _c[0],
              description: _c[1],
              rangeMin: parseInt(_c[2]),
              rangeMax: parseInt(_c[3]),
              unit: _c[4]
            });
          }
        });
      }
      const child: ActuatorNode = {
        name: actuator,
        attributes: {
          shortDescription: '',
          description: '',
          type: ActuatorType.analog,
          failSafe: FailSafeOptions.off,
          commands: command,
          feedback: feedback,
          id: generateObjectId()
        }
      };
      children.push(child);
    });
    unitNode.children = [...children];
  }
  return unitNode;
};

const getData = (sheet: WorkSheet) => {
  return XLSX.utils.sheet_to_json(sheet, { header: 1, blankrows: false }).splice(1);
};

type mapSetter = (value: MapofStringtoStringSet) => void;

export const loadIOList = (
  file: File,
  setUnitActuator: mapSetter,
  setActuatorCommand: mapSetter,
  setActuatorFeedback: mapSetter
) => {
  const unitActuator = new Map<string, Set<string>>();
  const actuatorCommand = new Map<string, Set<string>>();
  const actuatorFeedback = new Map<string, Set<string>>();

  const reader = new FileReader();

  const checkRow = (row: any) => {
    const unitName = row[0] + row[1];
    const hasActuator = row[2] !== undefined && row[5] !== undefined ? true : false;
    let existingActuators = unitActuator.get(unitName);
    existingActuators = existingActuators !== undefined ? existingActuators : new Set<string>();
    if (hasActuator) {
      const actuatorName = row[2] + row[5];
      existingActuators.add(actuatorName);
      if (commands.includes(row[3])) {
        let existingCommands = actuatorCommand.get(actuatorName);
        existingCommands = existingCommands !== undefined ? existingCommands : new Set<string>();
        if (_analogCommands.includes(row[3])) {
          existingCommands.add(
            row[3] + ':' + row[6] + ':' + row[10] + ':' + row[11] + ':' + row[12]
          );
        } else {
          existingCommands.add(row[3] + ':' + row[6]);
        }
        actuatorCommand.set(actuatorName, existingCommands);
      }
      if (feedback.includes(row[3])) {
        let existingFeedback = actuatorFeedback.get(actuatorName);
        existingFeedback = existingFeedback !== undefined ? existingFeedback : new Set<string>();
        if (_analogFeedback.includes(row[3])) {
          existingFeedback.add(
            row[3] + ':' + row[6] + ':' + row[10] + ':' + row[11] + ':' + row[12]
          );
        } else {
          existingFeedback.add(row[3] + ':' + row[6]);
        }
        actuatorFeedback.set(actuatorName, existingFeedback);
      }
    }
    unitActuator.set(unitName, existingActuators);
  };
  reader.onload = (e) => {
    const res: any = e.target?.result;
    const workbook = XLSX.read(res, { type: 'array' });
    const digitalInput = workbook.Sheets['DigitalInput'];
    const digitalInputData = getData(digitalInput);
    const digitalOutput = workbook.Sheets['DigitalOutput'];
    const digitalOutputData = getData(digitalOutput);
    const analogInput = workbook.Sheets['AnalogInput'];
    const analogInputData = getData(analogInput);
    const analogOutput = workbook.Sheets['AnalogOutput'];
    const analogOutputData = getData(analogOutput);
    digitalInputData.forEach((row) => checkRow(row));
    digitalOutputData.forEach((row) => checkRow(row));
    analogInputData.forEach((row) => checkRow(row));
    analogOutputData.forEach((row) => checkRow(row));
    setUnitActuator(unitActuator);
    setActuatorCommand(actuatorCommand);
    setActuatorFeedback(actuatorFeedback);
  };
  reader.readAsArrayBuffer(file);
};

export const dfs: any = (node: UnitNode, id: string) => {
  if (node.attributes.id === id) {
    return node;
  }
  for (const child in node.children) {
    // @ts-ignore
    if (node.children[child].children) {
      // @ts-ignore
      const x = dfs(node.children[child], id);
      if (x) return x;
    }
  }
  return null;
};

export const modifyNode: any = (node: UnitNode, newNode: ObjectNode, id: string) => {
  if (node.attributes.id === id) {
    node.name = newNode.name;
    //@ts-ignore
    node.attributes = newNode.attributes;
    return;
  }
  for (const child in node.children) {
    //@ts-ignore
    if (node.children[child].attributes.id === id) {
      //@ts-ignore
      node.children[child] = newNode;
      return;
    }
    //@ts-ignore
    if (node.children[child].children) {
      //@ts-ignore
      modifyNode(node.children[child], newNode, id);
    }
  }
  return null;
};

export const deleteNode: any = (node: UnitNode, id: string) => {
  if (node.attributes.id === id) {
    return node;
  }
  for (const child in node.children) {
    //@ts-ignore
    if (node.children[child]?.attributes.id === id) {
      node.children = node.children.filter((e) => e.attributes.id !== id);
    }
    //@ts-ignore
    if (node?.children[child]?.children) {
      //@ts-ignore
      const x = deleteNode(node.children[child], id);
      if (x) {
        node.children = node.children.filter((e) => e.attributes.id !== x.attributes.id);
      }
    }
  }
  return null;
};



export const addUnitToDB = async (node: UnitNode, parentId: string | null, projectId: string, addUnit : any) => { 
  const unit = {
    _id: node.attributes.id,
    parent: parentId,
    shortName: node.name,
    shortDescription: node.attributes.shortDescription,
    description: node.attributes.description,
    //@ts-ignore
    displayName: node.attributes.displayName
  };
  const variables = { projectId, unit };
  const res = await addUnit(variables);
  const { data, error } = res;
  if (error) {
    console.log(error.message);
  }
  return data;
}

export const addActuatorToDB = async (node: ActuatorNode, parentId: string | null, projectId: string, addActuator : any) => { 
  const actuator = {
    _id: node.attributes.id,
    parent: parentId,
    shortName: node.name,
    shortDescription: node.attributes.shortDescription,
    description: node.attributes.description,
    //@ts-ignore
    failSafe: node.attributes.failSafe,
    //@ts-ignore
    command: node.attributes.commands,
    //@ts-ignore
    feedback: node.attributes.feedback
  };
  const variables = { projectId, actuator };
  const res = await addActuator(variables);
  const { data, error } = res;
  if (error) {
    console.error(error.message);
  }
  return data;
}

