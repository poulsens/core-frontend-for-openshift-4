import React, { useState, useEffect } from 'react';
import { Button } from '../../../components/Button';
import { TextArea } from '../../../components/TextArea';
import { TextInput } from '../../../components/TextInput';
import { UnitNode } from '../../../models/hierarchy';
import { ButtonShape, ButtonType } from '../../../models/ui';
import { useGraph } from '../../../context/GraphContext';
import { addUnitNode } from './functions';
import { UnitsDropdown } from './UnitsDropdown';
import { isObjectEmpty } from '../../../utility/isObjectEmpty';
import { isObjectUnit } from '../../../utility/isObjectUnit';

export const UnitNodeForm: React.FC = () => {
  const {
    unitActuator,
    actuatorCommand,
    actuatorFeedback,
    createOrUpdateNodeSendEvent,
    createOrUpdateNodeState
  } = useGraph();

  const [shortName, setShortName] = useState('');
  const [displayName, setDisplayName] = useState('');
  const [shortDescription, setShortDescription] = useState('');
  const [description, setDescription] = useState('');

  const { selectedNode } = createOrUpdateNodeState.context;

  const addNode = () => {
    const node = addUnitNode(
      shortName,
      displayName,
      shortDescription,
      description,
      unitActuator,
      actuatorCommand,
      actuatorFeedback
    );
    createOrUpdateNodeSendEvent({ type: 'SUBMIT', item: node });
    clearState();
  };

  const editNode = () => {
    const childNode: UnitNode = {
      name: shortName,
      attributes: {
        id: selectedNode.attributes.id,
        displayName: displayName,
        shortDescription,
        description
      },
      children: selectedNode.children
    };
    createOrUpdateNodeSendEvent({ type: 'SUBMIT', item: childNode });
    clearState();
  };

  const clearState = () => {
    //@ts-ignore
    setShortName(null); //TODO: remove ts-ignore and fix the type issue by making this nullable probably?
    setDisplayName('');
    setShortDescription('');
    setDescription('');
  };

  useEffect(() => {
    const { selectedNode, isInEditMode } = createOrUpdateNodeState.context;
    if (
      selectedNode &&
      !isObjectEmpty(selectedNode) &&
      isObjectUnit(selectedNode) &&
      isInEditMode
    ) {
      const { name, attributes } = selectedNode;
      setShortName(name);
      setDisplayName(attributes?.displayName);
      setDescription(attributes?.description);
      setShortDescription(attributes?.shortDescription);
      console.log('selected a parent');
    } else if (
      selectedNode &&
      !isObjectEmpty(selectedNode) &&
      isObjectUnit(selectedNode) &&
      !isInEditMode
    ) {
      clearState();
    }
  }, [createOrUpdateNodeState.context.selectedNode]);

  return (
    <div className="flex flex-col">
      <UnitsDropdown setShortName={setShortName} shortName={shortName} />
      <TextInput
        id="displayName"
        placeholder="Display Name"
        value={displayName}
        setValue={setDisplayName}
      />
      <TextInput
        id="short_description"
        placeholder="Short Description"
        value={shortDescription}
        setValue={setShortDescription}
      />
      <TextArea
        id="description"
        placeholder="Description"
        value={description}
        setValue={setDescription}
      />
      {!createOrUpdateNodeState.context.isInEditMode ? (
        <Button
          type={ButtonType.secondary}
          shape={ButtonShape.rectangle}
          onClick={() => addNode()}
          text="Add"
        />
      ) : (
        <Button
          type={ButtonType.secondary}
          shape={ButtonShape.rectangle}
          onClick={() => editNode()}
          text="Edit"
        />
      )}
    </div>
  );
};
