import React, { useState, useEffect } from 'react';
import { UnitNodeForm } from './UnitNodeForm';
import { ProcessDecompositionGraph } from './ProcessDecompositionGraph';
import { Button } from '../../../components/Button';
import { ButtonShape, ButtonType } from '../../../models/ui';
import { addIcon, deleteIcon } from '../../../components/Icons';
import { useGraph } from '../../../context/GraphContext';
import { ActuatorNodeForm } from './ActuatorNodeForm';
import { DataInput, DragDropFile } from '../../../components/DragDropFile';
import { deleteNode, loadIOList } from './functions';
import { isObjectEmpty } from '../../../utility/isObjectEmpty';
import { CheckBox } from '../../../components/Checkbox';
import { FormHeader } from './FormHeader';
import { isObjectUnit } from '../../../utility/isObjectUnit';

export const ProcessDecompositionContent: React.FC = () => {
  const [showRootNodeForm, setShowRootNodeForm] = useState(true);
  const [showChildNodeForm, setShowChildNodeForm] = useState(false);
  const {
    setUnitActuator,
    setActuatorCommand,
    setActuatorFeedback,
    createOrUpdateNodeState,
    createOrUpdateNodeSendEvent
  } = useGraph();
  const [isUnit, setIsUnit] = useState(false);
  const { selectedNode, isInEditMode, hideForm } = createOrUpdateNodeState.context;

  const showRootNode = () => {
    createOrUpdateNodeSendEvent('ADD_ROOT_MODE');
    setShowChildNodeForm(false);
    setShowRootNodeForm(true);
  };

  const showChildNode = () => {
    if (selectedNode && !isObjectEmpty(selectedNode)) {
      createOrUpdateNodeSendEvent({ type: 'ADD_MODE', item: selectedNode });
      setIsUnit(false);
      setShowChildNodeForm(true);
      setShowRootNodeForm(false);
    } else {
      alert('Select a unit to add children!');
    }
  };

  const handleDelete = () => {
    createOrUpdateNodeSendEvent('DELETE');
  };

  useEffect(() => {
    if (selectedNode && !isObjectEmpty(selectedNode)) {
      if (isObjectUnit(selectedNode)) {
        setShowChildNodeForm(true);
        setShowRootNodeForm(false);
        setIsUnit(true);
      } else {
        setShowChildNodeForm(true);
        setShowRootNodeForm(false);
        setIsUnit(false);
      }
    }
  }, [selectedNode]);

  const handleFile = (file: File) => {
    loadIOList(file, setUnitActuator, setActuatorCommand, setActuatorFeedback);
  };

  return (
    <div className="flex flex-col w-full h-5/6 justify-between">
      <h1 className="mx-auto text-lg text-secondary py-2"> Process Decomposition </h1>
      <DragDropFile handleFile={handleFile}>
        <div>
          <DataInput handleFile={handleFile} />
        </div>
      </DragDropFile>
      <div className="flex flex-row p-4 h-full">
        <div className="flex flex-col w-2/3 h-full">
          <ProcessDecompositionGraph />
          <div className="flex flex-row-reverse p-4 w-full ">
            <Button
              shape={ButtonShape.rectangle}
              type={ButtonType.primary}
              onClick={showRootNode}
              text="Root Node"
              icon={addIcon}
            />
            {selectedNode && isObjectUnit(selectedNode) && (
              <Button
                shape={ButtonShape.rectangle}
                type={ButtonType.primary}
                onClick={showChildNode}
                text="Child Node"
                icon={addIcon}
              />
            )}
          </div>
        </div>
        {!hideForm && (showChildNodeForm || showRootNodeForm) && (
          <div className="w-1/2 flex-col p-2 h-5/6 overflow-y-scroll">
            <FormHeader
              title={
                showRootNodeForm
                  ? 'Add Root Node'
                  : isInEditMode
                  ? 'Node: ' + selectedNode?.name
                  : 'Child Node of ' + selectedNode?.name
              }
              handleDelete={handleDelete}
            />
            {showRootNodeForm && <UnitNodeForm />}
            {showChildNodeForm && (
              <div className="flex flex-col">
                {!isInEditMode && <CheckBox label="Unit" value={isUnit} setValue={setIsUnit} />}
                {isUnit ? <UnitNodeForm /> : <ActuatorNodeForm />}
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};
