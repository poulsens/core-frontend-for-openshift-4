import React from 'react';
import { Button } from '../../../components/Button';
import { deleteIcon } from '../../../components/Icons';
import { ButtonShape, ButtonType } from '../../../models/ui';
import { isObjectEmpty } from '../../../utility/isObjectEmpty';
import { useGraph } from '../../../context/GraphContext';

interface FormHeaderProps {
  title: string | null;
  handleDelete: () => void;
}

export const FormHeader: React.FC<FormHeaderProps> = ({ title, handleDelete }) => {
  const { createOrUpdateNodeState } = useGraph();
  const { selectedNode, isInEditMode } = createOrUpdateNodeState.context;
  return (
    <div className="flex justify-between w-full">
      <h1>{title}</h1>
      {!isObjectEmpty(selectedNode) && selectedNode && isInEditMode && (
        <Button
          shape={ButtonShape.rectangle}
          type={ButtonType.primary}
          icon={deleteIcon}
          onClick={handleDelete}
        />
      )}
    </div>
  );
};
