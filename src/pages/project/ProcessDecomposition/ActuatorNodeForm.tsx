import React, { useState, useEffect } from 'react';
import { Button } from '../../../components/Button';
import { CheckBox } from '../../../components/Checkbox';
import { TextArea } from '../../../components/TextArea';
import { TextInput } from '../../../components/TextInput';
import {
  ActuatorNode,
  ActuatorType,
  FailSafeOptions,
  DigitalCommand,
  AnalogCommand,
  DigitalFeedback,
  AnalogFeedback
} from '../../../models/hierarchy';
import { ButtonShape, ButtonType } from '../../../models/ui';
import { useGraph } from '../../../context/GraphContext';
import { _analogCommands, _analogFeedback, _digitalCommands, _digitalFeedback } from './functions';
import { ActuatorOptions } from './ActuatorOptions';
import { isObjectEmpty } from '../../../utility/isObjectEmpty';
import { generateObjectId } from '../../../utility/generateUuid';
import { isObjectUnit } from '../../../utility/isObjectUnit';

export const ActuatorNodeForm: React.FC = () => {
  const [shortName, setShortName] = useState('');
  const [displayName, setDisplayName] = useState('');
  const [shortDescription, setShortDescription] = useState('');
  const [description, setDescription] = useState('');
  const [failSafe, setFailSafe] = useState(FailSafeOptions.off);
  const [digitalCommands, setDigitalCommands] = useState<DigitalCommand[]>([]);
  const [analogCommands, setAnalogCommands] = useState<AnalogCommand[]>([]);
  const [digitalFeedback, setDigitalFeedback] = useState<DigitalFeedback[]>([]);
  const [analogFeedback, setAnalogFeedback] = useState<AnalogFeedback[]>([]);

  const { createOrUpdateNodeSendEvent, createOrUpdateNodeState } = useGraph();

  const { selectedNode } = createOrUpdateNodeState.context;

  const clearState = () => {
    setShortName('');
    setDisplayName('');
    setShortDescription('');
    setDescription('');
    setDigitalCommands([]);
    setAnalogCommands([]);
    setDigitalFeedback([]);
    setAnalogFeedback([]);
  };

  const addChildNode = () => {
    const childNode: ActuatorNode = {
      name: shortName,
      attributes: {
        shortDescription,
        description,
        type: ActuatorType.analog,
        failSafe: failSafe,
        commands: [...(digitalCommands || []), ...analogCommands],
        feedback: [...digitalFeedback, ...analogFeedback],
        id: generateObjectId()
      }
    };
    createOrUpdateNodeSendEvent({ type: 'SUBMIT', item: childNode });
    clearState();
  };

  const editChildNode = () => {
    const childNode: ActuatorNode = {
      name: shortName,
      attributes: {
        shortDescription,
        description,
        type: ActuatorType.analog,
        failSafe: failSafe,
        commands: [...(digitalCommands || []), ...(analogCommands || [])],
        feedback: [...(digitalFeedback || []), ...(analogFeedback || [])],
        id: selectedNode.attributes.id
      }
    };
    createOrUpdateNodeSendEvent({ type: 'SUBMIT', item: childNode });
    clearState();
  };

  useEffect(() => {
    const { selectedNode } = createOrUpdateNodeState.context;
    if (selectedNode && !isObjectEmpty(selectedNode) && !isObjectUnit(selectedNode)) {
      const { name, attributes } = selectedNode;
      setShortName(name);
      setDescription(attributes?.description);
      setShortDescription(attributes?.shortDescription);
      setDigitalCommands(
        attributes?.commands?.filter((c: DigitalCommand) => _digitalCommands.includes(c.name))
      );
      setAnalogCommands(
        attributes?.commands?.filter((c: AnalogCommand) => _analogCommands.includes(c.name))
      );
      setDigitalFeedback(
        attributes?.feedback?.filter((c: DigitalFeedback) => _digitalFeedback.includes(c.name))
      );
      setAnalogFeedback(
        attributes?.feedback?.filter((c: AnalogFeedback) => _analogFeedback.includes(c.name))
      );
      setFailSafe(attributes?.failSafe);
    }
  }, [createOrUpdateNodeState.context.selectedNode]);

  return (
    <div className="flex flex-col">
      <TextInput
        id="short_name"
        placeholder="Short Name"
        value={shortName}
        setValue={setShortName}
      />
      <TextInput
        id="short_description"
        placeholder="Short Description"
        value={shortDescription}
        setValue={setShortDescription}
      />
      <TextArea
        id="description"
        placeholder="Description"
        value={description}
        setValue={setDescription}
      />
      <ActuatorOptions
        digitalCommands={digitalCommands}
        setDigitalCommands={setDigitalCommands}
        analogCommands={analogCommands}
        setAnalogCommands={setAnalogCommands}
        digitalFeedback={digitalFeedback}
        setDigitalFeedback={setDigitalFeedback}
        analogFeedback={analogFeedback}
        setAnalogFeedback={setAnalogFeedback}
        failSafe={failSafe}
        setFailSafe={setFailSafe}
      />
      {!createOrUpdateNodeState.context.isInEditMode ? (
        <Button
          type={ButtonType.secondary}
          shape={ButtonShape.rectangle}
          onClick={addChildNode}
          text="Add"
        />
      ) : (
        <Button
          type={ButtonType.secondary}
          shape={ButtonShape.rectangle}
          onClick={editChildNode}
          text="Edit"
        />
      )}
    </div>
  );
};
