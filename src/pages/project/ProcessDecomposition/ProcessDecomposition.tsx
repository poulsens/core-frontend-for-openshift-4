import React from 'react';
import { GraphProvider } from '../../../context/GraphContext';
import { ProcessDecompositionContent } from './ProcessDecompositionContent';

export const ProcessDecomposition: React.FC = () => {
  return (
    <GraphProvider>
      <ProcessDecompositionContent />
    </GraphProvider>
  );
};
