import { useState, useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router';
import { SideBar } from '../../components/SideBar';
import { SideBarItemProps } from '../../models/ui';
import { DocHeader } from './DocHeader';
import { Introduction } from './Introduction';
import { ProcessDecomposition } from './ProcessDecomposition/ProcessDecomposition';
import { useProject } from '../../context/ProjectContext';
import { Units } from './units/Units';
import { AllUnitNames } from '../../queries/unit';
import { useQuery } from 'urql';
import { UnitDescription } from './units/Description';
import { OperationalStates } from './units/OperationalStates';
import { ObjectBehaviour } from './units/ObjectBehaviour';
import { UserCommands } from './units/UserCommands';
import { Parameters } from './units/Parameters';
import { ComputedVariables } from './units/ComputedVariables';
import { UnitAlarms } from './units/alarms/UnitAlarms';

export const NewProject = () => {
  const { projectName, projectId } = useProject();
  const cleanName = projectName.replace(/ /g, '');
  const [allUnits, reexecuteAllUnits] = useQuery({
    query: AllUnitNames,
    variables: {projectId}, 
    pause: projectId === null, 
  })
  const {data, error, fetching} = allUnits; 
  const newProjectItems: Array<SideBarItemProps> = [
    {
      title: 'Introduction',
      route: `/project/${cleanName}/introduction`
    },
    {
      title: 'Process Decomposition',
      route: `/project/${cleanName}/process-decomposition`
    },
    {
      title: 'Units',
      route: `/project/${cleanName}/units`,
      children: []
    },
    {
      title: 'Supervision',
      route: 'supervision'
    },
    {
      title: 'References',
      route: 'references'
    }
  ];

  const unitRoutes = (unitName: string) =>  [
    {
      title: 'Operational States',
      route: `/project/${cleanName}/units/${unitName}/operationalStates`
    }, 
    {
      title: 'Object Behaviour', 
      route: `/project/${cleanName}/units/${unitName}/objectBehaviour`
    }, 
    {
      title: 'Regulation Loops', 
      route: `/project/${cleanName}/units/${unitName}/regulationLoops`
    },
    {
      title: 'User Commands', 
      route: `/project/${cleanName}/units/${unitName}/userCommands`
    }, 
    {
      title: 'Parameters', 
      route: `/project/${cleanName}/units/${unitName}/parameters`
    }, 
    {
      title: 'Computed Variables', 
      route: `/project/${cleanName}/units/${unitName}/computedVariables`
    }, 
    {
      title: 'Unit Feedback', 
      route: `/project/${cleanName}/units/${unitName}/unitFeedback`
    }, 
    {
      title: 'Events', 
      route: `/project/${cleanName}/units/${unitName}/events`
    }, 
    {
      title: 'Alarms', 
      route: `/project/${cleanName}/units/${unitName}/alarms`, 
      children: alarmsRoutes(unitName)
    }
  ]

  const alarmsRoutes = (unitName: string) => [
    {
      title: 'Unit', 
      route: `/project/${cleanName}/units/${unitName}/alarms/unit`
    }, 
    {
      title: 'Actuator', 
      route: `/project/${cleanName}/units/${unitName}/alarms/actuator`
    }, 
    {
      title: 'CCC', 
      route: `/project/${cleanName}/units/${unitName}/alarms/ccc`
    }, 
  ]

  const [projectMenuItems, setProjectMenuItems] = useState(newProjectItems)

  useEffect(() => {
    if(data && data.plantById.units){
      const newMenu = projectMenuItems
      const unitsMenu = newMenu.filter(item => item.title === 'Units')
      data.plantById.units.forEach(
        (unit : {shortName: string, _id: string}) => {
          const existing = unitsMenu[0].children?.filter(child => child.title === unit.shortName)
          console.log(existing)
          if(existing?.length === 0)
            unitsMenu[0].children?.push({title: unit.shortName, route: `/project/${cleanName}/units/${unit.shortName}`, children: unitRoutes(unit.shortName)})}
      )
      setProjectMenuItems(newMenu)
    }
    
  }, [data])

  return (
    <div className="flex flex-column w-full h-full flex-wrap justify-between content-between">
      <DocHeader />
      <SideBar items={projectMenuItems} title="">
        <Switch>
          <Route exact path="/project/:id/introduction" component={Introduction} />
          <Route exact path="/project/:id/process-decomposition" component={ProcessDecomposition} />
          <Route exact path="/project/:id/units" component={Units} />
          <Route exact path="/project/:id/units/:unitName" component={UnitDescription} /> 
          <Route exact path="/project/:id/units/:unitName/operationalStates" component={OperationalStates} /> 
          <Route exact path="/project/:id/units/:unitName/objectBehaviour" component={ObjectBehaviour} /> 
          <Route exact path="/project/:id/units/:unitName/userCommands" component={UserCommands} />
          <Route exact path="/project/:id/units/:unitName/parameters" component={Parameters} />
          <Route exact path="/project/:id/units/:unitName/computedVariables" component={ComputedVariables} /> 
          <Route exact path="/project/:id/units/:unitName/alarms/"> 
            <Redirect to="/project/:id/units/:unitName/alarms/unit" />
          </Route>
          <Route exact path="/project/:id/units/:unitName/alarms/unit" component={UnitAlarms} />
        </Switch>
        <div className="flex flex-end w-full flex-row-reverse p-4 text-secondary bottom-0">
          Prev | Next
        </div>
      </SideBar>
    </div>
  );
};
