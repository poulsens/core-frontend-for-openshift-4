import React, { useState } from 'react';
import { useLocation, useParams } from 'react-router';
import { Dropdown } from '../../components/Dropdown';
import { useProject } from '../../context/ProjectContext';

const dropdownOptions = ['Viewing', 'Editing', 'Reviewing'];

export const DocHeader = () => {
  const [mode, setMode] = useState(dropdownOptions[1]);
  const { projectName } = useProject();
  return (
    <div className="flex flex-row justify-between w-screen p-4 border-b-2 h-1/6">
      <h1> {projectName} </h1>
      <Dropdown options={dropdownOptions} defaultValue={mode} setValue={setMode} label="Mode" />
    </div>
  );
};
