import { gql } from '@urql/core';

export const AllActuatorsQuery = gql`
  query( $projectId: MongoID!) {
    plantById(_id: $projectId) {
      _id
      actuators {
        _id
        shortName
        shortDescription
        description
        parent
        failSafe
        command {
          name
          description
          rangeMin
          rangeMax
          unit
          status
          labelOn
          labelOff
          _id
        }
        feedback {
          name
          description
          rangeMin
          rangeMax
          unit
          status
          _id
        }
      }
    }
  }
`;

export const GetChildActuatorsOfUnit = gql`
  query( $projectId: String!, $unitName: String!) {
    childActuatorsOfUnit(plantId: $projectId, unitName: $unitName) {
      _id, 
      shortName
    }
  }
`
