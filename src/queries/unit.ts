import { gql } from '@urql/core';

export const AllUnitsQuery = gql`
  query ($projectId: MongoID!) {
    plantById(_id: $projectId) {
      units {
        _id
        shortName
        displayName
        shortDescription
        description
        parent
      }
    }
  }
`;

export const AllUnitNames = gql`
query ($projectId: MongoID!) {
  plantById(_id: $projectId) {
    units {
      _id
      shortName
    }
  }
}
`;

export const GetUnitDescriptionByName = gql`
  query ($projectId: String!, $unitName: String!){
    unitByName(plantId: $projectId, unitName: $unitName){
      _id
      description 
    }
  }
`

export const GetOperationalStatesByUnitName = gql`
  query ($projectId: String!, $unitName: String!){
    unitByName(plantId: $projectId, unitName: $unitName){
      _id
      operationalStates {
        step 
        name 
        description
      }
    }
  }
`
export const GetOperationalStateNamesByUnitName = gql`
  query ($projectId: String!, $unitName: String!){
    unitByName(plantId: $projectId, unitName: $unitName){
      _id
      operationalStates {
        name 
      }
    }
  }
`

export const GetChildUnitsOfUnit = gql `
  query( $projectId: String!, $unitName: String!) {
    childUnitsOfUnit(plantId: $projectId, unitName: $unitName) {
      _id, 
      shortName
    }
  }
`


export const GetTransitionConditionsByUnitName = gql`
  query ($projectId: String!, $unitName: String!){
    unitByName(plantId: $projectId, unitName: $unitName){
      _id
      transitionConditions {
        start 
        stop 
        name 
        condition 
      }
    }
  }
`