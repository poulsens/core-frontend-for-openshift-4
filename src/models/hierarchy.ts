export type ObjectNode = UnitNode | ActuatorNode;

export interface UnitNodeAttributes {
  displayName: string;
  shortDescription: string;
  description: string;
  id: string;
}

export interface UnitNode {
  name: string;
  attributes: UnitNodeAttributes;
  children?: Array<ObjectNode>;
}

export enum ActuatorType {
  analog = 'ANALOG',
  digital = 'DIGITAL',
  combination = 'COMBINATION'
}

export enum FailSafeOptions {
  on = 'ON',
  off = 'OFF',
  doOn = '2 DO On',
  doOff = '2 DO Off'
}

export interface ActuatorNodeAttributes {
  id: string;
  shortDescription: string;
  description: string;
  type?: ActuatorType;
  failSafe: FailSafeOptions;
  commands: Array<Command>;
  feedback: Array<Feedback>;
}

export interface ActuatorNode {
  name: string;
  attributes: ActuatorNodeAttributes;
}

export interface _Command {
  name: string;
  description: string;
}

export interface AnalogCommand extends _Command {
  rangeMin: number;
  rangeMax: number;
  unit: string;
}

export interface DigitalCommand extends _Command {
  status: string;
  labelOn: string;
  labelOff: string;
}

export type Command = AnalogCommand | DigitalCommand;

export interface _Feedback {
  name: string;
  description: string;
}

export interface DigitalFeedback extends _Feedback {
  status: string;
}

export interface AnalogFeedback extends _Feedback {
  rangeMin: number;
  rangeMax: number;
  unit: string;
}

export type Feedback = DigitalFeedback | AnalogFeedback;
