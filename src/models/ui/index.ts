import { ReactElement } from 'react';
import { Descendant, BaseEditor } from 'slate';
import { ReactEditor } from 'slate-react';
import { UnitNode } from '../hierarchy';

export type CustomElement = { type: 'paragraph'; children: CustomText[] };
export type CustomText = { text: string };
declare module 'slate' {
  interface CustomTypes {
    Editor: BaseEditor & ReactEditor;
    Element: CustomElement;
    Text: CustomText;
  }
}

export enum ButtonType {
  primary = 'primary',
  secondary = 'secondary',
  transparent = 'transparent'
}

export enum ButtonState {
  active = 'active',
  inactive = 'inactive'
}

export enum ButtonShape {
  circle = 'circle',
  rectangle = 'rectangle',
  rounded_rectangle = 'rounded_rectangle'
}

export interface ButtonProps {
  text?: string;
  icon?: JSX.Element;
  shape: ButtonShape;
  type: ButtonType;
  onClick: () => void;
  disabled?: boolean;
}

export interface CheckboxProps {
  label: string;
  value: boolean;
  setValue: (e: boolean) => void;
}

export interface SideBarProps {
  title?: string;
  items: Array<SideBarItemProps>;
}

export interface SideBarItemProps {
  title: string;
  route: string;
  selected?: boolean;
  children?: Array<SideBarItemProps>;
}

export interface DropdownOption {
  readonly label: string;
  readonly value: string;
}

export interface TextInputProps {
  value: string;
  setValue: (e: string) => void;
  placeholder: string;
  id: string;
  size?: number;
}

export interface TextEditorProps {
  value: Descendant[];
  setValue: (e: Descendant[]) => void;
  placeholder: string;
}

export interface ModalProps {
  visibility: boolean;
  setVisibility: (e: boolean) => void;
  title: string;
  content: ReactElement;
  submitModal: () => void;
}

export interface ProcessDecompositionGraphProps {
  rootNodes: Array<UnitNode>;
}

export interface NodeFormProps {
  rootNodes: Array<UnitNode>;
  setRootNodes: (e: Array<UnitNode>) => void;
}
