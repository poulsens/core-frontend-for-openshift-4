import React, { useState } from 'react';
import { useMachine } from '@xstate/react';
import { ActuatorNode, UnitNode } from '../models/hierarchy';
import createOrUpdateNodeMachine, {
  CreateOrUpdateNodeMachineContext,
  CreateOrUpdateNodeMachineEvent
} from '../machines/CreateOrUpdateNodeMachine';
import { useMutation, useQuery } from 'urql';
import { AddUnitToProject, RemoveUnitInProject, UpdateUnitInProject } from '../mutations/unit';
import { isObjectEmpty } from '../utility/isObjectEmpty';
import { addActuatorToDB, addUnitToDB, deleteNode, dfs, modifyNode } from '../pages/project/ProcessDecomposition/functions';
import {
  AddActuatorToProject,
  RemoveActuatorInProject,
  UpdateActuatorInProject
} from '../mutations/actuator';
import { Sender } from 'xstate';
import { AllUnitsQuery } from '../queries/unit';
import { getStorageValue } from '../utility/getStorageValue';
import { useProject } from './ProjectContext';
import { Units } from '../pages/project/units/Units';
import { AllActuatorsQuery } from '../queries/actuator';

export type MapofStringtoStringSet = Map<string, Set<string>>;

type GraphProviderProps = { children: React.ReactNode };
type GraphContextData = {
  unitActuator: MapofStringtoStringSet;
  setUnitActuator: (value: MapofStringtoStringSet) => void;
  actuatorCommand: MapofStringtoStringSet;
  setActuatorCommand: (value: MapofStringtoStringSet) => void;
  actuatorFeedback: MapofStringtoStringSet;
  setActuatorFeedback: (value: MapofStringtoStringSet) => void;
  createOrUpdateNodeState: any;
  createOrUpdateNodeSendEvent: any;
};

const GraphContext = React.createContext<GraphContextData | undefined>(undefined);

function GraphProvider({ children }: GraphProviderProps) {
  const emptyMapStringtoStringSet1 = new Map<string, Set<string>>();
  const emptyMapStringtoStringSet2 = new Map<string, Set<string>>();
  const emptyMapStringtoStringSet3 = new Map<string, Set<string>>();
  const [addUnitResult, addUnit] = useMutation(AddUnitToProject);
  const [updateUnitResult, updateUnit] = useMutation(UpdateUnitInProject);
  const [removeUnitResult, removeUnit] = useMutation(RemoveUnitInProject);
  const [addActuatorResult, addActuator] = useMutation(AddActuatorToProject);
  const [updateActuatorResult, updateActuator] = useMutation(UpdateActuatorInProject);
  const [removeActuatorResult, removeActuator] = useMutation(RemoveActuatorInProject);

  const { projectId } = useProject();
  const [allUnits, reexecuteAllUnits] = useQuery({
    query: AllUnitsQuery,
    variables: {projectId}, 
    pause: !projectId || projectId === undefined, 
    requestPolicy: 'cache-and-network',
  });
  const [allActuators, reexecuteAllActuators] = useQuery({
    query: AllActuatorsQuery, 
    variables: {projectId}, 
    pause: !projectId || projectId === undefined,
    requestPolicy: 'cache-and-network',
  })

  const addChildUnits = (parentId : string, graph : UnitNode[]) => { 
    const { data } = allUnits
    const { units } = data.plantById; 
    const node = graph.map(node => dfs(node, parentId)).filter(node => node !== null)
    const childUnits = units.filter((unit: any) => unit.parent === parentId)
    if (childUnits === [])
      return; 
    childUnits.forEach((child: any) => {
      const unit : UnitNode = { 
        name: child.shortName, 
        attributes: {
          id: child._id,
          displayName: child.displayName,
          shortDescription: child.shortDescription,
          description: child.description
        },
        children: []
      }
      node[0].children.push(unit)
    });
    const res = childUnits.map((unit: any) => {
      addChildActuators(unit._id, graph); 
      addChildUnits(unit._id, graph); 
    })
    if (!res) return; 
  }

  const addChildActuators = (parentId: string, graph: UnitNode[]) => { 
    const node = graph.map(node => dfs(node, parentId)).filter(node => node !== null)
    const { data } = allActuators
    const { actuators } = data.plantById; 
    const childActuators = actuators.filter((actuator : any) => actuator.parent === parentId)
    if (childActuators === [])
      return; 
    childActuators.forEach((child: any) => { 
      const actuator : ActuatorNode = {
        name: child.shortName, 
        attributes : { 
          id: child._id, 
          shortDescription: child.shortDescription, 
          description: child.description, 
          failSafe: child.failSafe, 
          //@ts-ignore
          feedback: child.feedback.map(({__typename, _id , ...cleanAttrs}) => cleanAttrs ), 
          //@ts-ignore
          commands: child.command.map(({__typename, _id , ...cleanAttrs}) => cleanAttrs )
        }
      }
      node[0].children.push(actuator)
    })
  }

  const [createOrUpdateNodeState, createOrUpdateNodeSendEvent] = useMachine(
    createOrUpdateNodeMachine,
    {
      services: {
        fetchItem:
          (ctx: CreateOrUpdateNodeMachineContext) =>
          async (send: Sender<CreateOrUpdateNodeMachineEvent>) => {
            const { data, fetching, error } = allUnits;
            console.log(fetching, error)
            const newGraph: UnitNode[] = [];
            if( !fetching && !error){
              const { units } = data.plantById;
              if(!units){
                send({
                  //@ts-ignore
                  type: 'UPDATE_GRAPH',
                  //@ts-ignore
                  item: newGraph
                });
              }
              const rootUnits = units
                .filter((unit: any) => unit.parent === null); 
            if(rootUnits){
              rootUnits.forEach((node: any) => {
                const rootNode: UnitNode = {
                  name: node.shortName,
                  attributes: {
                    id: node._id,
                    displayName: node.displayName,
                    shortDescription: node.shortDescription,
                    description: node.description
                  },
                  children: []
                };
                newGraph.push(rootNode);
              });
              rootUnits.forEach((node : any) => {
                if(node){
                  addChildUnits(node._id, newGraph)
                  addChildActuators(node._id, newGraph)
                }
              });
            }
            console.log(newGraph)
            send({
              //@ts-ignore
              type: 'UPDATE_GRAPH',
              //@ts-ignore
              item: newGraph
            });
            }
            
          },
        createItem: (ctx: CreateOrUpdateNodeMachineContext, event) => async (send) => {
          const { graph, selectedNode, itemToProcess } = ctx;
          let newGraph: any[] = [];
          // const { projectId } = useProject();
          if (itemToProcess?.hasOwnProperty('children')) {
            //addUnitNode
            const parentId = selectedNode?.attributes.id ?? null
            const res = await addUnitToDB(itemToProcess as UnitNode, parentId, projectId, addUnit)
            if( (itemToProcess as UnitNode).children !== []) {
              (itemToProcess as UnitNode).children?.forEach(async child => {
                if(!child.hasOwnProperty('children')){
                  const res = await addActuatorToDB(child as ActuatorNode, itemToProcess.attributes.id, projectId, addActuator)
                }
              });
            }
            if (isObjectEmpty(selectedNode) || !selectedNode) {
              //adding Root Node
              newGraph = [...graph, itemToProcess];
              send({
                //@ts-ignore
                type: 'UPDATE_GRAPH',
                //@ts-ignore
                item: newGraph
              });
            } else {
              //adding Child Node
              const nodes: (UnitNode | null)[] = graph
                .map((node) => dfs(node, selectedNode.attributes.id))
                .filter((e) => e !== null);
              nodes[0]?.children?.push(itemToProcess);
              newGraph = [...graph];
            }
            send({
              //@ts-ignore
              type: 'UPDATE_GRAPH',
              //@ts-ignore
              item: newGraph
            });
          } else {
            //addActuatorNode
            if (selectedNode && itemToProcess) {
              const res = await addActuatorToDB(itemToProcess as ActuatorNode, selectedNode.attributes.id, projectId, addActuator)
              const node: (UnitNode | null)[] = graph
                .map((node) => dfs(node, selectedNode.attributes.id))
                .filter((e) => e !== null);
              node[0]?.children?.push(itemToProcess);
              const newGraph = [...graph];
              send({
                //@ts-ignore
                type: 'UPDATE_GRAPH',
                //@ts-ignore
                item: newGraph
              });
            }
          }
        },
        editItem: (ctx, event) => async (send) => {
          const { graph, itemToProcess, selectedNode } = ctx as CreateOrUpdateNodeMachineContext;
          const projectId = localStorage.getItem('projectId');
          console.log(event)
          console.log("editing")
          //@ts-ignore
          if (itemToProcess?.children) {
            const unit = {
              _id: itemToProcess.attributes.id,
              shortName: itemToProcess.name,
              shortDescription: itemToProcess.attributes.shortDescription,
              description: itemToProcess.attributes.description,
              //@ts-ignore
              displayName: itemToProcess.attributes.displayName,
              
            };
            const variables = { projectId, unit };
            const res = await updateUnit(variables);
            const { data, error } = res;
            if (error) {
              console.error(error);
            }
          } else if (itemToProcess) {
            console.log(itemToProcess)
            const actuator = {
              _id: itemToProcess.attributes.id,
              shortName: itemToProcess.name,
              shortDescription: itemToProcess.attributes.shortDescription,
              description: itemToProcess.attributes.description,
              //@ts-ignore
              failSafe: itemToProcess.attributes.failSafe,
              //@ts-ignore
              command: itemToProcess.attributes.command,
              //@ts-ignore
              feedback: itemToProcess.attributes.feedback
            };
            const variables = { projectId, actuator };
            const res = await updateActuator(variables);
            const { data, error } = res;
            if (error) {
              console.error(error);
            }
          }
          graph.map((node) => modifyNode(node, itemToProcess, selectedNode?.attributes.id));
          const newGraph = [...graph];
          send({
            type: 'UPDATE_GRAPH',
            item: newGraph
          });
        },
        deleteItem: (ctx, event) => async (send) => {
          const { graph, selectedNode, itemToProcess } = ctx as CreateOrUpdateNodeMachineContext;
          const id = selectedNode?.attributes.id;
          const projectId = localStorage.getItem('projectId');
          //@ts-ignore
          const res = selectedNode?.children ? await removeUnit({projectId, unitId: id}) : await removeActuator({projectId, actuatorId: id})
          const { data, error } = res;
          if (error) {
            console.error(error);
          }
          const nodes = graph.map((node) => deleteNode(node, id)).filter((e) => e !== null);
          let newGraph: UnitNode[];
          console.log('deleting');
          if (nodes[0]) {
            newGraph = graph.filter((e) => e.attributes.id !== nodes[0].attributes.id);
          } else {
            newGraph = [...graph];
          }
          send({
            //@ts-ignore
            type: 'UPDATE_GRAPH',
            //@ts-ignore
            item: newGraph
          });
        }
      }
    }
  );

  const [unitActuator, setUnitActuator] = useState<MapofStringtoStringSet>(
    emptyMapStringtoStringSet1
  );
  const [actuatorCommand, setActuatorCommand] = useState<MapofStringtoStringSet>(
    emptyMapStringtoStringSet2
  );
  const [actuatorFeedback, setActuatorFeedback] = useState<MapofStringtoStringSet>(
    emptyMapStringtoStringSet3
  );

  return (
    <GraphContext.Provider
      value={{
        unitActuator,
        setUnitActuator,
        actuatorCommand,
        setActuatorCommand,
        actuatorFeedback,
        setActuatorFeedback,
        createOrUpdateNodeState,
        createOrUpdateNodeSendEvent
      }}>
      {children}
    </GraphContext.Provider>
  );
}

function useGraph() {
  const context = React.useContext(GraphContext);
  if (context === undefined) {
    throw new Error('useGraph must be defined with a GraphProvider');
  }

  return context;
}

export { GraphProvider, useGraph };
