import React, { useState } from 'react';
import { useEffect } from 'react';

type ProjectProviderProps = { children: React.ReactNode };

type ProjectContextData = {
  projectName: string;
  projectId: string;
  setProjectName: (s: string) => void;
  setProjectId: (s: string) => void;
};

const ProjectContext = React.createContext<ProjectContextData | undefined>(undefined);

function ProjectProvider({ children }: ProjectProviderProps) {
  const _projectName = localStorage.getItem('projectName');
  const _projectId = localStorage.getItem('projectId');
  const [projectName, setProjectName] = useState(_projectName || '');
  const [projectId, setProjectId] = useState(_projectId || '');

  useEffect(() => {
    localStorage.setItem('projectName', projectName);
    localStorage.setItem('projectId', projectId);
  }, [projectName, projectId]);
  return (
    <ProjectContext.Provider value={{ projectName, projectId, setProjectName, setProjectId }}>
      {children}
    </ProjectContext.Provider>
  );
}

function useProject() {
  const context = React.useContext(ProjectContext);
  if (context === undefined) {
    throw new Error('useGraph must be defined with a GraphProvider');
  }

  return context;
}

export { ProjectProvider, useProject };
