import React, { useMemo, useState, useCallback } from 'react';
import { createEditor, Editor, Transforms, Element as SlateElement } from 'slate';
import { Slate, Editable, withReact, useSlate } from 'slate-react';
//@ts-ignore
import * as Latex from 'react-latex';
import isHotkey from 'is-hotkey';
import { Toolbar } from './Toolbar';
import { Element } from './Elements';
import { Leaf } from './Leaf';
import { HOTKEYS, LIST_TYPES } from './constants';
import { listIcon } from '../Icons';
import { TextEditorProps } from '../../models/ui';

export const TextEditor: React.FC<TextEditorProps> = ({ value, setValue, placeholder }) => {
  const [showToolbar, setShowToolbar] = useState(false);
  const renderElement = useCallback((props) => <Element {...props} />, []);
  const renderLeaf = useCallback((props) => <Leaf {...props} />, []);
  const editor = useMemo(() => withReact(createEditor()), []);
  const onKeyDown = (event: React.KeyboardEvent) => {
    for (const hotkey in HOTKEYS) {
      if (isHotkey(hotkey, event as any)) {
        event.preventDefault();
        //@ts-ignore
        const mark = HOTKEYS[hotkey];
        toggleMark(editor, mark);
      }
    }
  };
  return (
    <Slate editor={editor} value={value} onChange={(value) => setValue(value)}>
      <Editable
        onFocus={() => setShowToolbar(true)}
        onBlur={() => setShowToolbar(false)}
        renderElement={renderElement}
        renderLeaf={renderLeaf}
        placeholder={placeholder}
        onKeyDown={onKeyDown}
        className="w-full mt-2 px-3 border rounded-t-sm border-gray-300 pt-5 pb-2 focus:border-secondary focus:ring-1 focus:ring-primary focus:outline-none input active:outline-none"
      />
      {showToolbar && (
        <div className="bg-gray-100 ">
          <MarkButton format="bold" icon="B" />
          <MarkButton format="italic" icon="I" />
          <MarkButton format="underline" icon="U" />
          <BlockButton format="heading-three" icon="H3" />
          <BlockButton format="heading-four" icon="H4" />
        </div>
      )}
    </Slate>
  );
};

const toggleMark = (editor: Editor, format: string) => {
  const isActive = isMarkActive(editor, format);

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};

const toggleBlock = (editor: Editor, format: string) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && LIST_TYPES.includes(n.type),
    split: true
  });
  const newProperties: Partial<SlateElement> = {
    //@ts-ignore
    type: isActive ? 'paragraph' : isList ? 'list-item' : format
  };
  Transforms.setNodes(editor, newProperties);

  if (!isActive && isList) {
    const block = { type: format, children: [] };
    //@ts-ignore
    Transforms.wrapNodes(editor, block);
  }
};

const isBlockActive = (editor: Editor, format: string) => {
  const { selection } = editor;
  if (!selection) return false;

  //@ts-ignore
  const [match] = Editor.nodes(editor, {
    at: Editor.unhangRange(editor, selection),
    match: (n) => !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === format
  });

  return !!match;
};

const isMarkActive = (editor: Editor, format: string) => {
  const marks = Editor.marks(editor);
  //@ts-ignore
  return marks ? marks[format] === true : false;
};

const MarkButton = ({ format, icon }: { format: string; icon: JSX.Element | string }) => {
  const editor = useSlate();
  return (
    <button
      className={
        isMarkActive(editor, format) ? 'px-3 py-1 text-white bg-secondary' : 'px-3 py-1 text-black'
      }
      onMouseDown={(event) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}
    >
      {icon}
    </button>
  );
};

const BlockButton = ({ format, icon }: { format: string; icon: JSX.Element | string }) => {
  const editor = useSlate();
  return (
    <button
      className={
        isBlockActive(editor, format) ? 'px-3 py-1 text-white bg-secondary' : 'px-3 py-1 text-black'
      }
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      {icon}
    </button>
  );
};
