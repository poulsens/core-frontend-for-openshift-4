//@ts-ignore
import * as Latex from 'react-latex';
//@ts-ignore
export const Element = ({ attributes, children, element }) => {
  switch (element.type) {
    case 'block-quote':
      return <blockquote {...attributes}>{children}</blockquote>;
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>;
    case 'heading-one':
      return <h1 {...attributes}>{children}</h1>;
    case 'heading-two':
      return <h2 {...attributes}>{children}</h2>;
    case 'heading-three':
      // return <h3 {...attributes}>{children}</h3>
      return <Latex>What is $(3\times 4) \div (5-3)$</Latex>;
    case 'heading-four':
      return <h4 {...attributes}>{children}</h4>;
    case 'list-item':
      return <li {...attributes}>{children}</li>;
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>;
    default:
      return <p {...attributes}>{children}</p>;
  }
};
