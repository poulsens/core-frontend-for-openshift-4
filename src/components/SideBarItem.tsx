import { Link } from 'react-router-dom';
import { useState } from 'react';
import { SideBarItemProps } from '../models/ui';
import { styles } from '../styles';
import { dropdownIcon, upIcon } from './Icons';
import { MouseEventHandler } from 'react';

export const SideBarItem: React.FC<SideBarItemProps> = (item: SideBarItemProps) => {
  const [showChildren, setShowChildren] = useState(false);

  const icon = showChildren ? upIcon : dropdownIcon;

  const toggleChildren: MouseEventHandler<HTMLDivElement> = (e) => {
    setShowChildren(!showChildren);
  };

  return (
    <Link to={item.route}>
      <li
        className={
          styles.menu.item +
          ' ' +
          (item.selected ? styles.menu.itemActive : styles.menu.itemInactive)
        }
      >
        <div className="flex flex-row justify-between">
          {item.title}
          {item.children && <div onClick={(e) => toggleChildren(e)}> {icon} </div>}
        </div>
        {item.children && showChildren ? (
          <ul>
            {item.children.map((child: SideBarItemProps) => {
              return (
                <SideBarItem key={child.title} {...child} />
                // <Link to = {child.route}>
                //     <li className={styles.menu.item}>{child.title}</li>
                // </Link>
              );
            })}
          </ul>
        ) : null}
      </li>
    </Link>
  );
};
