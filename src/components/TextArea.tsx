import React, { useState } from 'react';
import { TextInputProps } from '../models/ui';

export const TextArea: React.FC<TextInputProps> = ({ id, value, setValue, placeholder }) => {
  const onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setValue(e.currentTarget.value);
  };
  return (
    <div className="mb-4 relative flex">
      <textarea
        rows={6}
        x-model={id}
        id={id}
        value={value}
        onChange={(e) => onChange(e)}
        placeholder={placeholder}
        className="w-full rounded px-3 border border-gray-300 pt-5 pb-2 focus:border-secondary focus:ring-1 focus:ring-primary focus:outline-none input active:outline-none"
        autoFocus
      />
      <label
        htmlFor={id}
        className="label absolute mt-2 ml-3 leading-tighter text-gray-600 text-base cursor-text"
      >
        {placeholder}
      </label>
    </div>
  );
};
