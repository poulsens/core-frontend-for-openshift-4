import React from 'react';
import { CheckboxProps } from '../models/ui';

export const CheckBox: React.FC<CheckboxProps> = ({ label, value, setValue }) => {
  const onClick = () => {
    setValue(!value);
  };
  return (
    <label className="inline-flex py-5">
      <input
        className="text-primary w-8 h-8 mr-3 focus:ring-secondary focus:ring-opacity-25 border border-gray-300 rounded"
        type="checkbox"
        // checked={value}
        onClick={onClick}
        defaultChecked={value}
      />
      {label}
    </label>
  );
};
