import { Link } from 'react-router-dom';

export interface TitleCardProps {
  title: string;
  route: string;
  id: string;
}

export const TitleCard: React.FC<TitleCardProps> = ({ title, route, id }) => {
  return (
    <Link to={route}>
      <div className="w-44 h-56 shadow-lg drop-shadow-lg m-2 border-2 border-secondary">
        <p className="text-lg"> {title} </p>
      </div>
    </Link>
  );
};
