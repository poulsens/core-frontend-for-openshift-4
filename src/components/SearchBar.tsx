import { TextInputProps } from '../models/ui';

export const SearchBar: React.FC<TextInputProps> = ({ value, setValue, placeholder }) => {
  const onSearch = (e: React.FormEvent<HTMLInputElement>) => {
    setValue(e.currentTarget.value);
    console.log(value);
  };
  return (
    <input
      type="text"
      placeholder={placeholder}
      value={value}
      onChange={onSearch}
      className="border-0 border-b-2 focus:border-secondary focus:ring-0 focus:outline-none w-800"
    ></input>
  );
};
