import React, { useState } from 'react';
import { ButtonProps, ButtonState } from '../models/ui';
import { styles } from '../styles';

export const Button: React.FC<ButtonProps> = ({ text, onClick, shape, icon, type, disabled }) => {
  const [state, setState] = useState(ButtonState.active);
  return (
    <button
      onClick={onClick}
      className={
        styles.button['normal'] +
        ' ' +
        styles.button[type] +
        ' ' +
        styles.button[state] +
        ' ' +
        styles.button[shape]
      }
      disabled={disabled}>
      {icon}
      {text}
    </button>
  );
};
