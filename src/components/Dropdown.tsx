interface DropdownProps {
  label: string;
  options: Array<string>;
  defaultValue: string;
  setValue: (e: string) => void;
}

export const Dropdown: React.FC<DropdownProps> = ({ label, options, setValue, defaultValue }) => {
  const changeDropdownValue = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setValue(e.target.value);
  };
  return (
    <div className="mb-4">
      <label> {label} </label>
      <select
        value={defaultValue}
        onChange={(e) => changeDropdownValue(e)}
        className="rounded px-3 border border-gray-300 pt-2 pb-2 focus:border-secondary focus:ring-1 focus:ring-primary focus:outline-none active:outline-none"
      >
        {options.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};
