import { useState } from 'react';
import ContentEditable, { ContentEditableEvent } from 'react-contenteditable';
import { addIconBlack, deleteIcon } from './Icons';
import '../styles/Table.css';
import { generateObjectId } from '../utility/generateUuid';

enum TableSize {
  sm,
  md,
  lg
}

interface TableColumn {
  index: string;
  title: string;
  width?: string;
  type?: string;
}
interface TableProps {
  size: TableSize;
  items: Array<any>;
  cols: Array<TableColumn>;
  setItems: (items: []) => void;
  onEdit?: () => void; 
}

const onOffRadioOptions = [
  {
    title: 'ON',
    value: 'on'
  },
  {
    title: 'OFF',
    value: 'off'
  }
];
export const Table: React.FC<TableProps> = ({ size, items, cols, setItems }) => {
  const emptyRow: any = {};
  cols.map((col) => {
    emptyRow[col.index] = '';
  });
  const [row, setRow] = useState(emptyRow);

  const handleContentEditable = (e: ContentEditableEvent) => {
    const {
      currentTarget: {
        dataset: { column }
      },
      target: { value }
    } = e;
    setRow({ ...row, [column]: value });
  };

  const handleRadioChange = (value: any, column: string) => {
    setRow({ ...row, [column]: value.target.value });
  };

  const addRow = () => {
    const newItems = [...(items ?? []), row] as any; //TODO: remove this and fix type issue
    setItems(newItems);
    setRow(emptyRow);
  };
  const deleteRow = (item: any) => {
    const newItems = items.filter((i) => i !== item) as any; //TODO: remove this!
    setItems(newItems);
  };

  return (
    <table className="border my-2 divide-y divide-gray-300 table-fixed">
      <thead className="bg-secondary bg-opacity-50">
        <tr className="p-2">
          <th></th>
          {cols.sort().map((col) => (
            <th className="p-2" key={col.index}>
              {col.title}
            </th>
          ))}
        </tr>
      </thead>
      <tbody className="bg-white divide-y divide-gray-300">
        {items?.map((item) => (
          <tr key={generateObjectId()}>
            <td
              onClick={() => deleteRow(item)}
              className="p-2 hover:bg-primary hover:bg-opacity-25 border">
              {deleteIcon}
            </td>
            {cols.sort().map((col) => (
              <td onChange={(e) => console.log(e)} className="border px-1" key={generateObjectId()}>
                {item[col.index]}
              </td>
            ))}
          </tr>
        ))}
        <tr>
          <td onClick={addRow} className="p-2 hover:bg-primary hover:bg-opacity-25 border px-1">
            {addIconBlack}
          </td>
          {
            //TODO: clean up this code if possible
            cols.sort().map((col, index) => (
              <td className="border px-1" key={index}>
                {col.type === 'onOffRadio' ? (
                  <div className="flex items-center">
                    {onOffRadioOptions.map((option) => (
                      <div
                        key={option.title}
                        onChange={(e) => handleRadioChange(e, col.index)}
                        className="flex items-center mr-4 mb-4">
                        <input
                          id={option.title}
                          type="radio"
                          name={col.index}
                          defaultChecked={option.value === row[col.index]}
                          value={option.value}
                        />
                        <label
                          htmlFor={option.title}
                          className="flex items-center cursor-pointer text-sm">
                          {option.title}
                        </label>
                      </div>
                    ))}
                  </div>
                ) : (
                  <ContentEditable
                    html={row[col.index]}
                    data-column={col.index}
                    onChange={handleContentEditable}
                    className="content-editable"
                  />
                )}
              </td>
            ))
          }
        </tr>
      </tbody>
    </table>
  );
};
