interface PageTitleProps { 
    title: string
}
export const PageTitle: React.FC<PageTitleProps> = ({title}) => { 
    return <h1 className="mx-auto text-secondary text-lg py-2"> {title} </h1>
}