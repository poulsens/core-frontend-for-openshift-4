import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { SideBarProps } from '../models/ui';
import { SideBarItem } from './SideBarItem';

export const SideBar: React.FC<SideBarProps> = ({ children, title, items }) => {
  const history = useHistory();
  const checkActive = (route: string) => {
    const path = history.location.pathname;
    if (path == route) return true;
    return false;
  };
  return (
    <div className="flex flex-row min-w-full h-5/6">
      <aside className="bg-primary  min-h-screen flex flex-col rounded-r-xl w-1/6">
        <p className="text-center text-2xl py-4 text-white">{title}</p>
        <div className="py-4 flex-grow relative">
          <nav>
            <ul>
              {items.map((item) => (
                <SideBarItem key={item.route} {...item} selected={checkActive(item.route)} />
              ))}
            </ul>
          </nav>
        </div>
      </aside>
      <div className="w-full">{children}</div>
    </div>
  );
};
