import { TextInputProps } from '../models/ui';
import '../styles/TextInput.css';

export const TextInput: React.FC<TextInputProps> = ({ id, value, setValue, placeholder, size }) => {
  const onChange = (e: React.FormEvent<HTMLInputElement>) => {
    setValue(e.currentTarget.value);
  };
  return (
    <div className="mb-4 relative">
      <input
        id={id}
        type="text"
        value={value}
        onChange={onChange}
        maxLength={size}
        required
        placeholder={placeholder}
        className={
          'w-full rounded px-3 border border-gray-300 pt-5 pb-2 focus:border-secondary focus:ring-1 focus:ring-primary focus:outline-none input active:outline-none' +
          (value !== '' && 'filledInputLabel')
        }
        autoFocus
      />
      <label
        htmlFor={id}
        className={
          'label absolute mt-2 ml-3 leading-tighter text-gray-600 text-base cursor-text' +
          (value !== '' && 'filledInputLabel')
        }
      >
        {placeholder}
      </label>
    </div>
  );
};
