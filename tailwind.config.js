module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#3DA9FC',
        secondary: '#094067'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
